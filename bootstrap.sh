#!/usr/bin/env bash

apt-get update
if ! hash apache2 ; then
  echo "Installing Apache.."
  #Instalo Apache2
  apt-get install -y apache2
  #Borro el contenido por defecto que trae apache
  rm -rf /var/www
  #Genero el link simbolico con mi carpeta
  ln -fs /vagrant /var/www
fi
if ! hash git ; then
  echo "Installing Git.."
  apt-get install -y git
fi
#Clono o hago un pull
echo "Cloning or updateting project.."
git clone https://github.com/chrodriguez/arquitecturas-de-software.git /var/www/html 2> /dev/null || git -C /var/www/html pull